import React from 'react'

function Tasks(props) {
  // props are passed from App as {...this.state}
  return <h1>{`Hello, ${props.user.name}!`}</h1>
}

export default Tasks
