import React from 'react'
import { Tasks } from './components'

const defaultState = { loading: true }

// hard-coded for now, but can be passed anything and set on state
const data = { user: { name: 'Scott', role: 'manager' }, component: 'tasks' }

class App extends React.Component {
  constructor() {
    super()
    this.state = defaultState
  }

  fetchData() {
    // later this can be an API request to some backend, or
    // can get passed data from the native app
    return data
  }

  componentDidMount() {
    const data = this.fetchData()
    this.setState({ loading: false, ...data })
  }

  renderLoading() {
    return <div>Loading...</div>
  }

  renderError() {
    return <div>Something went wrong. Please try again.</div>
  }
  renderTasks() {
    // {...this.state} passes everything on state to the component
    return <Tasks {...this.state} />
  }
  render() {
    if (this.state.loading) return this.renderLoading()
    if (this.state.error) return this.renderError()
    if (this.state.component === 'tasks') return this.renderTasks()
    return <div />
  }
}

export default App
